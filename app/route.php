<?php

$app->get('/accueil', function(){
    $c = new \src\controller\VueController();
    $c->afficherAccueil();
});

$app->get('/', function(){
    $c = new \src\controller\VueController();
    $c->afficherAccueil();
} );

$app->get('/td2', function(){
    $c = new \src\controller\VueController();
    $c->afficherTD(2);
} );

$app->get('/td3', function(){
    $c = new \src\controller\VueController();
    $c->afficherTD(3);
} );

$app->get('/td4', function(){
    $c = new \src\controller\VueController();
    $c->afficherTD(4);
} );

$app->get('/td5', function(){
    $c = new \src\controller\VueController();
    $c->afficherTD(5);
} );