<?php

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;


$app = new \Slim\Slim();

$db = new DB();
$tab = parse_ini_file('db.config.ini');
$db->addConnection($tab);
$db->setAsGlobal();
$db->bootEloquent();

require 'route.php';

$app->run();
