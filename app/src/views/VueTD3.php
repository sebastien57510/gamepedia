<?php

namespace src\views;

use src\models\Character;
use src\models\Game;
use src\models\GameRating;

class VueTD3 extends Vue
{

    public function __construct()
    {
        Vue::__construct();
    }

    public function genererContent() {
        $this->content =
        '<section>
            <div class="container">
                <h2>TD2</h2>';

        //Question 1
        $this->content.='<h3><u>1) Afficher les personnages du jeu 12342:</u></h3><div class="scrolling-overflow">';
        $this->content.='<table border="1"><tbody><tr><th>Id</th><th>Nom</th></tr>';

        $game = Game::find(12342);
        $chars = $game->characters;
        foreach ($chars as $c) {
            $this->content.='<tr><td>'.$c->id.'</td><td id="game-name">'.$c->name.'</td></tr>';
        }
        $this->content.='</tbody></table></div>';

        //Question 2
        $this->content.='<h3><u>2) Afficher les personnages des jeux dont le nom débute par Mario:</u></h3><div class="scrolling-overflow">';
        $this->content.='<table border="1"><tbody><tr><th>Id</th><th>Nom</th></tr>';

        $chars = Character::whereHas('game', function($q) {
            $q->where('name' , 'like', 'Mario%' ) ;
        })->get();
        foreach ($chars as $c) {
            $this->content.='<tr><td>'.$c->id.'</td><td id="game-name">'.$c->name.'</td></tr>';
        }
        $this->content.='</tbody></table></div>';

        //Question 3
        $this->content.='<h3><u>3) Afficher les jeux développés par une compagnie dont le nom contient Sony:</u></h3><div class="scrolling-overflow">';
        $this->content.='<table border="1"><tbody><tr><th>Id</th><th>Nom</th></tr>';

        //Temps de chargement trop long

        /*$game = Game::whereHas('company', function($q) {
            $q->where('name' , 'like', '%Sony%' ) ;
        })->get();
        foreach ($game as $g) {
            $this->content.='<tr><td>'.$g->id.'</td><td id="game-name">'.$g->name.'</td></tr>';
        }*/
        $this->content.='</tbody></table></div>';

        //Question 4
        $this->content.='<h3><u>4) Afficher le rating des jeux dont le nom contient Mario:</u></h3><div class="scrolling-overflow">';
        $this->content.='<table border="1"><tbody><tr><th>Id</th><th>Nom</th><th>Rating Board Deck</th></tr>';

        $list = GameRating::whereHas('game', function($q) {
            $q->where('name' , 'like', '%Mario%' ) ;
        })->get();
        foreach ($list as $c) {
            $this->content.='<tr><td>'.$c->id.'</td><td id="game-name">'.$c->name.'</td><td>'.$c->board->deck.'</td></tr>';
        }
        $this->content.='</tbody></table></div>';

        //Question 5
        $this->content.='<h3><u>5) Les jeux dont le nom commence par Mario et qui ont plus de 3 persos:</u></h3><div class="scrolling-overflow">';
        $this->content.='<table border="1"><tbody><tr><th>Id</th><th>Nom</th></tr>';

        $list = Game::where('name', 'like', 'Mario%')->get();
        foreach ($list as $c) {
            if (count($c->characters)>3) {
                $this->content .= '<tr><td>' . $c->id . '</td><td id="game-name">' . $c->name . '</td></tr>';
            }
        }
        $this->content.='</tbody></table></div>';

        //Question 6
        $this->content.='<h3><u>6) Les jeux dont le nom commence par Mario et dont le rating contient "3+":</u></h3><div class="scrolling-overflow">';
        $this->content.='<table border="1"><tbody><tr><th>Id</th><th>Nom</th><th>Rating</th></tr>';

        $list = Game::where('name', 'like', 'Mario%')->get();
        foreach ($list as $c) {
            $rate = $c->rating;
            foreach ($rate as $r) {
                if (strpos($r, '3+')) {
                    $this->content .= '<tr><td>' . $c->id . '</td><td id="game-name">' . $c->name . '</td><td>' . "$r->name" . '</td></tr>';
                }
            }
        }
        $this->content.='</tbody></table></div>';

        //Question 7
        $this->content.='<h3><u>7) Les jeux dont le nom commence par Mario publies par une compgnie dont le nom contient "Inc" et dont le rating contient "3+":</u></h3><div class="scrolling-overflow">';
        $this->content.='<table border="1"><tbody><tr><th>Id</th><th>Nom</th><th>Company</th><th>Rating</th></tr>';

        $list = Game::where('name', 'like', 'Mario%')->get();
        foreach ($list as $c) {
            $comps = $c->company;
            $cond1 = false;
            $comp = null;
            foreach ($comps as $comp) {
                if (strpos($comp, 'Inc.')) {
                    $cond1 = true;
                    break;
                }
            }

            $rate = $c->rating;
            foreach ($rate as $r) {
                if (strpos($r, '3+') && $cond1) {
                    $this->content .= '<tr><td>' . $c->id . '</td><td id="game-name">' . $c->name . '</td><td>' . $comp->name . '</td><td>' . "$r->name" . '</td></tr>';
                    break;
                }
            }
        }
        $this->content.='</tbody></table></div>';


        $this->content .= '</div></section>';
    }
}