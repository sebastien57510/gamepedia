<?php

namespace src\views;

use Slim\Slim;
use src\models\Client;

abstract class Vue
{
    protected $content,$slim, $par, $uri;


    public function __construct(){
        $this->uri = Slim::getInstance()->request()->getRootUri();

    }
    public function rendre() {
        $menu ='
                <ul class="nav navbar-nav">
                   <tr>
                        <li>
                            <a href="' . $this->uri . '/accueil">
                                Accueil
                            </a>
                        </li>
                        <li>
                            <a href="' . $this->uri . '/td2">
                                TD2
                            </a>
                        </li>
                        <li>
                            <a href="' . $this->uri . '/td3">
                                TD3
                            </a>
                        </li>
                        <li>
                            <a href="' . $this->uri . '/td4">
                                TD4
                            </a>
                        </li>
                        <li>
                            <a href="' . $this->uri . '/td5">
                                TD5
                            </a>
                        </li>
                        <li>
                            <a href="' . $this->uri . '/td6">
                                TD6
                            </a>
                        </li>
                   </tr>
                </ul>';

        return
'<!DOCTYPE html>
    <html>
        <head>
            <title>Gamepedia</title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="">
                <meta name="author" content="">

                <link href="' . $this->uri . '/web/css/common.css" rel="stylesheet">
                <link href="' . $this->uri . '/web/css/bootstrap.min.css" rel="stylesheet">
                <link href="' . $this->uri . '/web/css/font-awesome.min.css" rel="stylesheet">
                <link href="' . $this->uri . '/web/css/animate.min.css" rel="stylesheet">
                <link href="' . $this->uri . '/web/css/prettyPhoto.css" rel="stylesheet">
                <link href="' . $this->uri . '/web/css/main.css" rel="stylesheet">
                <link href="' . $this->uri . '/web/css/responsive.css" rel="stylesheet">
        </head>

        <body class="homepage">

            <div class="wrapper">
                <header id="header">

                    <nav class="navbar navbar-inverse" role="banner">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="'. $this->uri .'/accueil">Gamepedia</a>
                            </div>

                            <div class="collapse navbar-collapse navbar-right">
                                '. $menu .'
                            </div>
                        </div><!--/.container-->
                    </nav><!--/nav-->

                </header><!--/header-->
                ' . $this->content  . '

            </div>


        <footer id="footer" class="midnight-blue">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        &copy; 2016 IUT Nancy Charlemagne
                    </div>
                </div>
            </div>
        </footer><!--/#footer-->
        </body>
    </html>';
    }

    public abstract function genererContent();
}