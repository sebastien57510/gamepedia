<?php

namespace src\views;


use src\models\Company;
use src\models\Game;
use src\models\Platform;

class VueTD2 extends Vue
{

    public function __construct()
    {
        Vue::__construct();
    }

    public function genererContent() {
        //Tete de vue
        $this->content =
        '<section>
            <div class="container">
                <h2>TD2</h2>
                <h3><u>1) Lister les jeux avec Eloquent:</u></h3>
                ';

        //Question 1
        $list = Game::all();
        //$list = array();

        $this->content.='<div class="scrolling-overflow"><table border="1"><tbody><tr><th>Id</th><th>Nom</th><th>Deck</th></tr>';
        foreach ($list as $game) {
            $this->content.='<tr><td>'.$game->id.'</td><td id="game-name">'.$game->name.'</td><td>'.$game->deck.'</td></tr>';
        }
        $this->content.='</tbody></table></div>';

        //Question 2
        $list= Game::skip(21172)->take(442)->get();
        $this->content.='<h3><u>2) Lister 442 jeux a partir du 21173eme:</u></h3><div class="scrolling-overflow"><table border="1"><tbody><tr><th>Id</th><th>Nom</th><th>Deck</th></tr>';
        foreach ($list as $game) {
            $this->content.='<tr><td>'.$game->id.'</td><td id="game-name">'.$game->name.'</td><td>'.$game->deck.'</td></tr>';
        }
        $this->content.='</tbody></table></div>';

        //Question 3
        $list = Game::where('name','like','%Mario%')->get();
        $this->content.='<h3><u>3) Lister les jeux dont le nom contient Mario:</u></h3><div class="scrolling-overflow"><table border="1"><tbody><tr><th>Id</th><th>Nom</th><th>Deck</th></tr>';
        foreach ($list as $game) {
            $this->content.='<tr><td>'.$game->id.'</td><td id="game-name">'.$game->name.'</td><td>'.$game->deck.'</td></tr>';
        }
        $this->content.='</tbody></table></div>';

        //Question 4
        $list = Company::where('location_country','like','Japan')->get();
        $this->content.='<h3><u>4) Lister les compagnies installées au Japon:</u></h3><div class="scrolling-overflow"><table border="1"><tbody><tr><th>Id</th><th>Nom</th><th>Pays</th></tr>';
        foreach ($list as $comp) {
            $this->content.='<tr><td>'.$comp->id.'</td><td>'.$comp->name.'</td><td>'.$comp->location_country.'</td></tr>';
        }
        $this->content.='</tbody></table></div>';

        //Question 5
        $list = Platform::where('install_base','>=','10000000')->get();
        $this->content.='<h3><u>5) Lister les plateformes dont les bases installées sont >= à 10M:</u></h3><div class="scrolling-overflow"><table border="1"><tbody><tr><th>Id</th><th>Nom</th><th>Installées</th></tr>';
        foreach ($list as $plat) {
            $this->content.='<tr><td>'.$plat->id.'</td><td>'.$plat->name.'</td><td>'.$plat->install_base.'</td></tr>';
        }
        $this->content.='</tbody></table></div>';

        //Queue de vue
        $this->content.='
            </div>
        </section>';
    }
}