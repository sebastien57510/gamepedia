<?php

namespace src\views;

class VueAccueil extends Vue
{

    public function __construct()
    {
        Vue::__construct();
    }

    public function genererContent() {
        $this->content =
        '<section class="container">
            accueil
        </section>';
    }
}