<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 15/02/2016
 * Time: 11:22
 */

namespace src\controller;


use src\views\VueAccueil;
use src\views\VueTD2;
use src\views\VueTD3;
use src\views\VueTD4;
use src\views\VueTD5;

class VueController
{
    public function afficherAccueil() {
        $vueAccueil = new VueAccueil();
        $vueAccueil->genererContent();
        echo $vueAccueil->rendre();
    }

   public function afficherTD($td) {
       switch ($td) {
           case 2:
               $vue = new VueTD2();
               break;
           case 3:
               $vue = new VueTD3();
               break;
           case 4:
               $vue = new VueTD4();
               break;
           case 5:
               $vue = new VueTD5();
               break;
           default:
               $vue = new VueAccueil();
       }
       $vue->genererContent();
       echo $vue->rendre();
   }
}