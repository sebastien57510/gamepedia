<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 24/03/2016
 * Time: 11:18
 */

namespace src\models;


use Illuminate\Database\Eloquent\Model;

class GameRating extends Model
{
    protected $table = 'game_rating';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function board()
    {
        return $this->belongsTo('src\models\Ratingboard','rating_board_id');
    }
    public function game()
    {
        return $this->belongsToMany('src\models\Game','game2rating','rating_id','game_id');
    }
}