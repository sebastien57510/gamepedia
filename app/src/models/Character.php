<?php

namespace src\models;

use Illuminate\Database\Eloquent\Model;

class Character extends Model {

	protected $table = 'character';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function game()
	{
		return $this->belongsTo('src\models\Game','first_appeared_in_game_id');
	}

}