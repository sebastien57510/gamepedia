<?php

namespace src\models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model {

	protected $table = 'game';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function characters()
	{
		return $this->belongsToMany('src\models\Character','game2character','game_id','character_id');
	}

	public function company()
	{
		return $this->belongsToMany('src\models\Company','game_developers','game_id','comp_id');
	}

	public function rating()
	{
		return $this->belongsToMany('src\models\GameRating','game2rating','game_id','rating_id');
	}

}