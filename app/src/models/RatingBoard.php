<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 24/03/2016
 * Time: 11:24
 */

namespace src\models;


use Illuminate\Database\Eloquent\Model;

class RatingBoard extends Model
{
    protected $table = 'rating_board';
    protected $primaryKey = 'id';
    public $timestamps = false;
}