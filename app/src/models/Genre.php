<?php


namespace src\models;

use Illuminate\Database\Eloquent\Model;

class genre extends Model
{

        protected $table = 'genre';
        protected $primaryKey = 'id';
        public $timestamps = false;

}
?>
